	.syntax unified
	.cpu cortex-m4
	.eabi_attribute 27, 1
	.fpu fpv4-sp-d16
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 1
	.eabi_attribute 30, 4
	.eabi_attribute 34, 1
	.eabi_attribute 18, 4
	.thumb
	.syntax unified
	.file	"project0_tudo.c"
	.section	.text.NmiSR,"ax",%progbits
	.align	1
	.thumb
	.thumb_func
	.type	NmiSR, %function
NmiSR:
	@ Volatile: function does not return.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
.L2:
	b	.L2
	.size	NmiSR, .-NmiSR
	.section	.text.FaultISR,"ax",%progbits
	.align	1
	.thumb
	.thumb_func
	.type	FaultISR, %function
FaultISR:
	@ Volatile: function does not return.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
.L4:
	b	.L4
	.size	FaultISR, .-FaultISR
	.section	.text.IntDefaultHandler,"ax",%progbits
	.align	1
	.thumb
	.thumb_func
	.type	IntDefaultHandler, %function
IntDefaultHandler:
	@ Volatile: function does not return.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
.L6:
	b	.L6
	.size	IntDefaultHandler, .-IntDefaultHandler
	.section	.text.startup.main,"ax",%progbits
	.align	1
	.global	main
	.thumb
	.thumb_func
	.type	main, %function
main:
	@ Volatile: function does not return.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	push	{r3, lr}
	ldr	r0, .L10
	bl	SysCtlClockSet
	ldr	r0, .L10+4
	bl	SysCtlPeripheralEnable
	movs	r1, #14
	ldr	r0, .L10+8
	bl	GPIOPinTypeGPIOOutput
.L8:
	movs	r2, #2
	movs	r1, #14
	ldr	r0, .L10+8
	bl	GPIOPinWrite
	ldr	r0, .L10+12
	bl	SysCtlDelay
	movs	r2, #4
	movs	r1, #14
	ldr	r0, .L10+8
	bl	GPIOPinWrite
	ldr	r0, .L10+12
	bl	SysCtlDelay
	movs	r2, #8
	movs	r1, #14
	ldr	r0, .L10+8
	bl	GPIOPinWrite
	ldr	r0, .L10+12
	bl	SysCtlDelay
	b	.L8
.L11:
	.align	2
.L10:
	.word	29361472
	.word	536870944
	.word	1073893376
	.word	2000000
	.size	main, .-main
	.section	.text.ResetISR,"ax",%progbits
	.align	1
	.global	ResetISR
	.thumb
	.thumb_func
	.type	ResetISR, %function
ResetISR:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	push	{r3, lr}
	ldr	r2, .L16
	ldr	r3, .L16+4
	ldr	r1, .L16+8
.L13:
	cmp	r3, r1
	bcs	.L15
	ldr	r0, [r2, #4]!
	str	r0, [r3], #4
	b	.L13
.L15:
	.syntax unified
@ 237 "project0_tudo.c" 1
	    ldr     r0, =_bss
    ldr     r1, =_ebss
    mov     r2, #0
    .thumb_func
zero_loop:
        cmp     r0, r1
        it      lt
        strlt   r2, [r0], #4
        blt     zero_loop
@ 0 "" 2
	.thumb
	.syntax unified
	ldr	r2, .L16+12
	ldr	r3, [r2]
	orr	r3, r3, #15728640
	str	r3, [r2]
	bl	main
.L17:
	.align	2
.L16:
	.word	_etext-4
	.word	_data
	.word	_edata
	.word	-536810104
	.size	ResetISR, .-ResetISR
	.global	g_pfnVectors
	.section	.isr_vector,"a",%progbits
	.align	2
	.type	g_pfnVectors, %object
	.size	g_pfnVectors, 620
g_pfnVectors:
	.word	pulStack+256
	.word	ResetISR
	.word	NmiSR
	.word	FaultISR
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	0
	.word	0
	.word	0
	.word	0
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	0
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	0
	.word	0
	.word	0
	.word	0
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	0
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.word	IntDefaultHandler
	.section	.bss.pulStack,"aw",%nobits
	.align	2
	.type	pulStack, %object
	.size	pulStack, 256
pulStack:
	.space	256
	.ident	"GCC: (GNU Tools for ARM Embedded Processors) 5.4.1 20160919 (release) [ARM/embedded-5-branch revision 240496]"
