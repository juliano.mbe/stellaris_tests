	.file	"main.s"
	.syntax unified



@ @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

        .section	.text.portF_handler,"ax",%progbits
        .global portF_handler
	.align	1
	.thumb
	.thumb_func
	.type	portF_handler, %function
        
portF_handler:

        PUSH    {r4-r11,lr}

        LDR     r4, =GPIO_PORTF_ICR
        MOV     r5, #1
        STR     r5, [r4]

        ADD     r3, r3, #1

        CMP     r3, #25
        BLT     fim_h

        MOV     r3, #0
        
        LDR     r4, =GPIO_PORTF_DATA_R
        STR     r2, [r4]
        MOV     r2, r2, LSL #1

        AND     r2, r2, r1
        CMP     r2, #0
        BNE     fim_h

        MOV     r2, #2

fim_h:  
        
        POP     {r4-r11,pc}
        
	.size	portF_handler, .-portF_handler










        @ @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

        .section	.text.startup.main,"ax",%progbits
	.align	1
	.global	main
	.thumb
	.thumb_func
	.type	main, %function
        
main:
	@ Volatile: function does not return.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0

	push	{r3, lr}
        
	@ ldr	r0, .L10
	@ bl	SysCtlClockSet

        @ ldr	r0, .L10+4
	@ bl	SysCtlPeripheralEnable
        
	@ movs	r1, #14
	@ ldr	r0, .L10+8
	@ bl	GPIOPinTypeGPIOOutput


        @ ativa clock para porta F
        LDR r0, =SYSCTL_RCGC2_R  @ SYSCTL_RCGC2_R |= 0x20 
        LDR r1, [r0]
        MOV r2, #0x20
        ORR r1, r1, r2
        STR r1, [r0]

        @ unlock na porta F
        LDR r0, =GPIO_PORTF_LOCK_R  @ GPIO_PORTF_LOCK_R = 0x4C4F434B
        LDR r1, =0x4C4F434B
        STR r1, [r0]

        @ Habilita as mudancas da PF4-0
        LDR r0, =GPIO_PORTF_CR_R  @ GPIO_PORTF_CR_R = 0x0E
        MOV r1, #0x1F
        STR r1, [r0]

        @ Disabilita funcao analogica
        LDR r0, =GPIO_PORTF_AMSEL_R
        MOV r1, #0
        STR r1, [r0]

        @ Escolhe a funcao da porta para GPIO
        LDR r0, =GPIO_PORTF_PCTL_R
        STR r1, [r0]

        @ Se zerado so funciona como GPIO, senao eh a func escolhida no PCTL
        LDR r0, =GPIO_PORTF_AFSEL_R
        STR r1, [r0]

        @ ativa os resistores de Pull up de cada pino
        LDR r0, =GPIO_PORTF_PUR_R
        MOV r1, #0x11
        STR r1, [r0]

        @ ativa o digital I/O da PF4-0
        LDR r0, =GPIO_PORTF_DEN_R
        MOV r1, #0x1F
        STR r1, [r0]
        
        @ edita direcionamento dos pinos da porta
        LDR     r0, =GPIO_PORTF_DIR_R   @ endereco para direcionar GPIO PORT F
        MOV     r1, #14                @ 01110 (0:in, 1:out)
        STR     r1, [r0]               @ escreve o direcionamento

        @@@@  CONFIGURAR INTERRUPCAO PORTA F @@@@@@@@@@@@

        @ habilita interrupcao porta F
        LDR r0, =NVIC_EN0
        LDR r1, =0x40000000  @ set bit 30
        STR r1, [r0]

        @ seta a prioridade da porta F para 5
        @ NVIC_PRI7 = (NVIC_PRI7 & 0xFF00FFFF) | 0x00A00000
        LDR r0, =NVIC_PRI7
        LDR r1, =0xFF00FFFF
        LDR r2, =0x00A00000
        LDR r3, [r0]
        AND r3, r3, r1
        ORR r3, r3, r2
        STR r3, [r0]

        @@@@@@@
        
        @ tipo de trigger (0:edge, 1:level)
        LDR r4, =GPIO_PORTF_IS
        MOV r5, #0x1
        STR r5, [r4]

        @ se sera detectado com ambas bordas
        LDR r4, =GPIO_PORTF_IBE
        MOV r5, #0
        STR r5, [r4]
        
        @ 0: falling edge |  1: rising edge
        LDR r4, =GPIO_PORTF_IEV
        STR r5, [r4]

        @ marcara para habilitar
        LDR r4, =GPIO_PORTF_IM
        MOV r5, #0x01
        STR r5, [r4]

        @
        LDR r4, =GPIO_PORTF_RIS

        @
        LDR r4, =GPIO_PORTF_MIS 

        @
        LDR     r4, =GPIO_PORTF_ICR
        MOV     r5, #1
        STR     r5, [r4]

        
        
        @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


        MOV     r2, #2
        MOV     r1, #14
        MOV     r3, #0

loop_inf:
        
        B loop_inf
        
@ @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@2        
        
alt_led:
        MOV     r1, #14
        MOV     r3, #2                 @ dado para a porta F
                    
lal:        
        AND     r3, r3, r1
        CMP     r3, #0
        BEQ     alt_led        
        
        LDR     r0, =GPIO_PORTF_DATA_R  @ endereco para escrever porta F
        STR     r3, [r0]               @ escreve o dado

lr:     
        LDR     r0, =GPIO_PORTF_DATA_R
        LDR     r2, [r0]
        AND     r2, r2, #0x10

        CMP     r2, #0x10
        BEQ     lr      

        
@	ldr	r0, .L10+12
@	bl	SysCtlDelay

        MOV     r3 , r3, LSL #1

        B       lal


	.align	2
.L10:
	.word	29361472
	.word	536870944
	.word	1073893376
	.word	20000000

        
.set GPIO_PORTF_DIR_R,   0x40025400  @ GPIO_PORTF_DIR_R
.set GPIO_PORTF_DATA_R,  0x400253FC  @ GPIO_PORTF_DATA_R
        
.set SYSCTL_RCGC2_R,     0x400FE108
.set GPIO_PORTF_LOCK_R,  0x40025520
.set GPIO_PORTF_CR_R,    0x40025524
.set GPIO_PORTF_AMSEL_R, 0x40025528
.set GPIO_PORTF_PCTL_R,  0x4002552C
.set GPIO_PORTF_AFSEL_R, 0x40025420
.set GPIO_PORTF_PUR_R,   0x40025510
.set GPIO_PORTF_DEN_R,   0x4002551C
        

.set NVIC_EN0,        0xE000E100  @ set 0 bit 30 para int. porta F       
.set NVIC_PRI7,       0xE000E41C  @ seta prioridade para 31,30,29,28
        
.set GPIO_PORTF_IS,   0x40025404  
.set GPIO_PORTF_IBE,  0x40025408
.set GPIO_PORTF_IEV,  0x4002540C
.set GPIO_PORTF_IM,   0x40025410
.set GPIO_PORTF_RIS,  0x40025414
.set GPIO_PORTF_MIS,  0x40025418
.set GPIO_PORTF_ICR,  0x4002541C
        
	.size	main, .-main


@ @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        
